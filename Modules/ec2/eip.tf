resource "aws_eip" "jenkins_eip" {

   instance = aws_instance.jenkins_server.id

   vpc      = true

   tags = {
      Name = "jenkins_eip"
   }
}