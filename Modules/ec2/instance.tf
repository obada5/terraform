resource "aws_security_group" "ec2_sg" {

  name = var.sg_name
  description = var.sg_description
  vpc_id = var.vpc_id

  ingress {
    description = "Allow all traffic through port 8080"
    from_port = "8080"
    to_port = "8080"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow all traffic through port 22"
    from_port = "22"
    to_port = "22"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port = "0"
    to_port = "0"
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
    
}




resource "aws_instance" "jenkins_server" {

   ami = data.aws_ami.ubuntu.id

   instance_type = var.instance_type

   vpc_security_group_ids = [aws_security_group.ec2_sg.id]

   key_name = var.key

   user_data = "${file("./install_jenkins.sh")}"

   tags = {
      Name = var.instance_name
   }
}
