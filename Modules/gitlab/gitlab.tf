provider "gitlab" {
  token = var.token
}

resource "gitlab_project" "project" {
  name = var.project_name
}

resource "gitlab_branch" "staging" {
  name    = "staging"
  ref     = "main"
  project = gitlab_project.project.id
}