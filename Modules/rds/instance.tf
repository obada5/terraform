resource "aws_security_group" "sg" {

  name = var.sg_name
  description = var.sg_description
  vpc_id = var.vpc_id

  ingress {
    description = "Allow all traffic through port 3306"
    from_port = "3306"
    to_port = "3306"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port = "0"
    to_port = "0"
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  
  
}



resource "aws_db_instance" "laravel_db" {
  allocated_storage    = var.allocated_storage
  db_name              = var.db_name
  engine               = "mysql"
  engine_version       = "8.0.28"
  instance_class       = var.instance_class
  username             = var.username
  password             = var.password
  parameter_group_name = "default.mysql8.0"
  vpc_security_group_ids = [aws_security_group.sg.id]
  publicly_accessible = true
  skip_final_snapshot  = true
}