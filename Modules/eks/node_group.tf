resource "aws_iam_role" "node_group_role" {
    name = var.group_role_name
    assume_role_policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
        {
            Action = "sts:AssumeRole"
            Effect = "Allow"
            Sid    = ""
            Principal = {
            Service = "ec2.amazonaws.com"
            }
        },
        ]
    })
    managed_policy_arns=["arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy","arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"]
}


resource "aws_eks_node_group" "laravel_node_group" {
  cluster_name    = aws_eks_cluster.laravel_eks.name
  node_group_name = var.group_name
  node_role_arn   = aws_iam_role.node_group_role.arn
  subnet_ids = ["subnet-0b2f94a81f6fd5238","subnet-09a57d8a22e43b80c","subnet-02f2c44acb3e2a6fa"]

  scaling_config {
    desired_size = 2
    max_size     = 2
    min_size     = 2
  }

  instance_types=[var.instance_type]

  update_config {
    max_unavailable = 1
  }
}