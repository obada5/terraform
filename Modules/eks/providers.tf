terraform {
  required_providers {
    gitlab = {
        source = "gitlabhq/gitlab"
    }
    aws = {
      source = "hashicorp/aws"
    }
  }
}