resource "aws_iam_role" "eks_role" {
    name = var.role_name
    assume_role_policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
        {
            Action = "sts:AssumeRole"
            Effect = "Allow"
            Sid    = ""
            Principal = {
            Service = "eks.amazonaws.com"
            }
        },
        ]
    })
    managed_policy_arns=["arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"]
}

resource "aws_eks_cluster" "laravel_eks" {
    name=var.cluster_name
    role_arn = aws_iam_role.eks_role.arn
    vpc_config {
      endpoint_private_access=true
      subnet_ids = ["subnet-0b2f94a81f6fd5238","subnet-09a57d8a22e43b80c","subnet-02f2c44acb3e2a6fa"]
    }
}

