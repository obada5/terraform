variable "ec2_sg_name" {

}

variable "ec2_sg_description" {

}

variable "ec2_vpc_id" {

    }

variable "ec2_instance_type" {

    }


variable "ec2_key" {

    }

variable "ec2_instance_name" {

    }


variable "eks_role_name" {

}

variable "eks_group_role_name" {

}

variable "eks_cluster_name" {

}

variable "eks_group_name" {

}


variable "eks_instance_type" {

}

variable "rds_sg_name" {

  
}

variable "rds_sg_description" {
    
  
}

variable "rds_vpc_id" {
    
  
}

variable "rds_allocated_storage" {
  
}


variable "rds_db_name" {
  
}


variable "rds_instance_class" {
  
}

variable "rds_username" {
  
}


variable "rds_password" {
  
}

variable "gitlab_token" {
  
}

variable "gitlab_project_name" {
  
}

