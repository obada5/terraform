module "gitlab" {
  source = "./Modules/gitlab"
  token = var.gitlab_token
  project_name = var.gitlab_project_name
}

module "jenkins_ec2" {
  source = "./Modules/ec2"
  sg_name = var.ec2_sg_name
  sg_description = var.ec2_sg_description
  vpc_id = var.ec2_vpc_id
  instance_type = var.ec2_instance_type
  key = var.ec2_key
  instance_name = var.ec2_instance_name

}

module "eks" {
  source = "./Modules/eks"
  cluster_name = var.eks_cluster_name
  role_name = var.eks_role_name
  group_name = var.eks_group_name
  group_role_name = var.eks_group_role_name
  instance_type = var.eks_instance_type
}

module "rds" {
  source = "./Modules/rds"
  allocated_storage = var.rds_allocated_storage
  db_name = var.rds_db_name
  instance_class = var.rds_instance_class
  sg_description = var.rds_sg_description
  sg_name = var.rds_sg_name
  username = var.rds_username
  password = var.rds_password
  vpc_id = var.rds_vpc_id
}